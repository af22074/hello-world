import java.util.Scanner;

public class practice {
    public static void main(String[] args) {
        Scanner userIn = new Scanner(System.in);
        int select;
        String foodName="";
        System.out.println("What would you like to order:");
        System.out.println("1. Tempura");
        System.out.println("2. Ramen");
        System.out.println("3. Udon");
        System.out.print("Your order [1-3]:");
        select = userIn.nextInt();
        userIn.close();
        switch (select) {
            case 1:
                foodName = "Tempura";
                break;
            case 2:
                foodName = "Ramen";
                break;
            case 3:
                foodName = "Udon";
                break;
        }
        System.out.print("You have ordered " +foodName+". Thank you!");
    }
}
